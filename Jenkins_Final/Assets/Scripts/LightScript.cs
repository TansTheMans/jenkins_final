﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LightScript : MonoBehaviour
{
    //public GameObject LightImage;
    public Slider lightSlider;
    public int lightAmount;
    public Text lightText;
    public string lightSay;

    private void Start()
    {
        //LightImage = GameObject.Find("LightImage");

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            gameObject.GetComponent<SpriteRenderer>().enabled = true;
            lightSlider.value -= 1f;
        }

        if (Input.GetKeyUp(KeyCode.W))
        {
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
        }

        if (lightSlider.value <= 0)
        {
            Invoke("RestartLevel", 3f);
            lightText.gameObject.SetActive(true);
            lightText.text = lightSay;
        }

    }

    void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
