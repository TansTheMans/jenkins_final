﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HintScript : MonoBehaviour
{
    public Text hintText;

    public string toSay;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            hintText.gameObject.SetActive(true);
            hintText.text = toSay;
        }
    }



    private void OnTriggerExit2D(Collider2D other)
    {
       if (other.gameObject.CompareTag("Player"))
        {
            hintText.gameObject.SetActive(false);
        } 
    }


}
