﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileDestroy : MonoBehaviour
{
    public float timeToDestroy;

    private void Start()
    {
        Invoke("DestroyMe", timeToDestroy);
    }

    private void DestroyMe()
    {
        Destroy(gameObject);
    }
}
