﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CleanSoundScript : MonoBehaviour
{
    public AudioClip cleanSound;
    AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        audioSource.Play();
    }



}
