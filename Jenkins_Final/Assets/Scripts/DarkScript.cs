﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DarkScript : MonoBehaviour
{
    private bool canClean;
    public static int cleanCount;


    private void Update()
    {
        //with the bool activated, the instance of the object is destroyed and adds to a global counter
        if (canClean && Input.GetKey(KeyCode.W))
        {
            Destroy(gameObject);
            cleanCount += 1;
            print(cleanCount);
        }

        //loads the win screen when all instances are de-spawned
        if (cleanCount >= 5)
        {
            SceneManager.LoadScene(2);
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        //detects if the object in its collider is the player, then makes the instance ABLE to be destroyed with a press of the 'w' key
        if (collision.gameObject.name.Equals("Player"))
        {
            canClean = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //once the player has left the collider area, it can no longer be destroyed by pressing the 'w' key
        if (collision.gameObject.name.Equals("Player"))
        {
            canClean = false;
        }
    }
}
