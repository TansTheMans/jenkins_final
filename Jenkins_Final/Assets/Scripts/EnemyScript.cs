﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    public GameObject DemonSprite;
    private bool leftPlz = true;
    private bool rightPlz = false;
    

    void Start()
    {
        DemonSprite.gameObject.name = "DemonSprite";
    }

    void Update()
    {
        if (leftPlz == true)
        {
            DemonSprite.transform.position = new Vector3(DemonSprite.transform.position.x - 0.1f, DemonSprite.transform.position.y, DemonSprite.transform.position.z);
        }

        if (rightPlz == true)
        {
            DemonSprite.transform.position = new Vector3(DemonSprite.transform.position.x + 0.1f, DemonSprite.transform.position.y, DemonSprite.transform.position.z);
        }

        if (DemonSprite.transform.position.x <= -7f)
        {
            rightPlz = true;
            leftPlz = false;
        }

        if (DemonSprite.transform.position.x >= 7f)
        {
            rightPlz = false;
            leftPlz = true;

        }
    }
}
