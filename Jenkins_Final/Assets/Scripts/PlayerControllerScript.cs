﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerControllerScript : PhysicsScript
{
    public float maxSpeed = 7;
    public float jumpTakeOffSpeed = 7;
    public Slider healthBar;
    public Animator animator;

    private SpriteRenderer spriteRenderer;
    public int health;

    AudioSource audioSource;


    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        DarkScript.cleanCount = 0;
    }

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    protected override void ComputeVelocity()
    {
        Vector2 move = Vector2.zero;

        move.x = Input.GetAxis("Horizontal");
        animator.SetFloat("speed", Mathf.Abs(move.x));

        if (Input.GetButtonDown("Jump") && grounded)
        {
            velocity.y = jumpTakeOffSpeed;
            audioSource.Play();
        }
        else if (Input.GetButtonUp("Jump"))
        {
            if (velocity.y > 0)
            {
                velocity.y = velocity.y * 0.5f;
            }
        }
        targetVelocity = move * maxSpeed;

        if (targetVelocity.x > .01f)
            spriteRenderer.flipX = false;
        else
            if (targetVelocity.x < -.01f)
            spriteRenderer.flipX = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Projectile")
        {
            health -= 1;
            print(health);
            healthBar.value -= 1;
        }

        if (health <= 0)
        {
            Destroy(gameObject);
            print("health dead");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}