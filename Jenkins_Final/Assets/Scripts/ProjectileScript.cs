﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileScript : MonoBehaviour
{
    public GameObject projectile;
    public Transform parent;

    private void Start()
    {
        InvokeRepeating("LaunchProjectile", 1.0f, 1.0f);
        print("launched");
    }

    private void LaunchProjectile()
    {
        GameObject instance = Instantiate(projectile, transform.position, projectile.transform.rotation);
        //projectile.transform.position = parent.transform.position;
    }
}
